/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	// menu
	wp.customize( 'menu_backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default' ).css( {
					'backgroundColor': to,
				} );
				$( '.navbar-default' ).css( {
					'border-color': to
				} );		} );
	} );
	wp.customize( 'menucolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li > a' ).css( {
					'color': to
				} );
				
		} );
	} );
	wp.customize( 'menu_hovercolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li:hover' ).css( {
					'backgroundColor': to
				} );
		} );
	} );


	wp.customize( 'bodycolor', function( value ) {
		value.bind( function( to ) {
				$( '#main' ).css( {
					'color': to
				} );
		} );
	} );

	wp.customize( 'sidebar_textcolor', function( value ) {
		value.bind( function( to ) {
				$( '#secondary' ).css( {
					'color': to
				} );
		} );
	} );
	wp.customize( 'footercolor', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer, .site-footer .fa' ).css( {
					'color': to
				} );
		} );
	} );


	// main background color.
	wp.customize( 'body_backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '#main' ).css( {
					'backgroundColor': to,
				} );
		} );
	} );
	// main background color.
	wp.customize( 'sidebar_backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '#secondary, #content' ).css( {
					'backgroundColor': to,
				} );
		} );
	} );
	// main background color.
	wp.customize( 'footer_backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer' ).css( {
					'backgroundColor': to,
				} );
		} );
	} );



} )( jQuery );
