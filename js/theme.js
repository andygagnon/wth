/**
 * Chesterton
 * http://underscores.me
 *
 * Theme javascript
 */

(function ($) {
  'use strict';

  	// make wp_nav_menu compatible with bootstrap dropdowns
	$( "li.menu-item-has-children" ).addClass( "dropdown" );
	$( "li.menu-item-has-children > a").addClass( "dropdown-toggle" );
	$( "li.menu-item-has-children > a").attr('data-toggle', 'dropdown');
	$( "ul.sub-menu" ).addClass( "dropdown-menu" );
	$( "ul.sub-menu" ).addClass( "dropdown-menu-left" );


})(jQuery);
