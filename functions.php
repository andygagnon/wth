<?php
/**
 * WTH functions and definitions
 *
 * @package WTH
 */

define( "LOCAL_IP_ADDR", '10.0.2.15');


if ( ! function_exists( 'wth_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wth_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WTH, use a find and replace
	 * to change 'wth' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'wth', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'wth' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
/*	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );*/

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wth_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // wth_setup
add_action( 'after_setup_theme', 'wth_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function wth_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'wth' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'wth_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wth_scripts() {

	if ( WP_DEBUG)
		$version = '4'.date( '.YmdGi'); // for DEBUG
	else
		$version = null;

	// WP required CSS
  	wp_enqueue_style( 'wth-style-css', get_stylesheet_uri() ); // style.css

	// bootstrap css framework
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css', array(), $version);
	wp_enqueue_style( 'font-awesome-css', 'http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css', array(), null);
	// site CSS
	wp_enqueue_style( 'main-css', get_template_directory_uri() . '/css/main.css', array( 'wth-style-css', 'bootstrap-css'), $version);

  	// move jquery to footer
    if (!is_admin())
    {
        wp_deregister_script('jquery');
        // Load the copy of jQuery that comes with WordPress in to footer
        wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', false, $version, true);
        wp_enqueue_script('jquery');
	}

	// for watch in Gruntfile.js
	if ( in_array( $_SERVER['SERVER_ADDR'], array( '127.0.0.1', LOCAL_IP_ADDR)) || pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION) == 'dev') {
    	wp_enqueue_script( 'livereload', '//localhost:35729/livereload.js', '', false, true );
	}

	// TBD: use minified js
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), $version, true);
  	wp_enqueue_script( 'wth-theme-js', get_template_directory_uri() . '/js/theme.js', array('jquery'), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'wth_scripts' );

/**
 * Remove emoji support
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';
/**
 * Custom google fonts
 */
//require get_template_directory() . '/inc/google-font.php';

/**
 * Custom theme options
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom theme options
 */
require get_template_directory() . '/inc/custom-theme-options.php';

/**
 * Utility functions
 */
// set up this global for some functions
$sep = '/';
$dl_filepath = dirname(dirname(dirname(dirname(__FILE__)))) . $sep . 'wp-content' . $sep . 'uploads';
require get_template_directory() . '/inc/util.php';

/**
 * Contact Form class
 */
//require get_template_directory() . '/inc/class.contact.php';

/**
 * Custom post types
 */
require get_template_directory() . '/post-types/meat.php';

