<?php

function meat_init() {
	register_post_type( 'meat', array(
		'labels'            => array(
			'name'                => __( 'Meats', 'wth' ),
			'singular_name'       => __( 'Meat', 'wth' ),
			'all_items'           => __( 'Meats', 'wth' ),
			'new_item'            => __( 'New meat', 'wth' ),
			'add_new'             => __( 'Add New', 'wth' ),
			'add_new_item'        => __( 'Add New meat', 'wth' ),
			'edit_item'           => __( 'Edit meat', 'wth' ),
			'view_item'           => __( 'View meat', 'wth' ),
			'search_items'        => __( 'Search meats', 'wth' ),
			'not_found'           => __( 'No meats found', 'wth' ),
			'not_found_in_trash'  => __( 'No meats found in trash', 'wth' ),
			'parent_item_colon'   => __( 'Parent meat', 'wth' ),
			'menu_name'           => __( 'Meats', 'wth' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
	) );

}
add_action( 'init', 'meat_init' );

function meat_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['meat'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Meat updated. <a target="_blank" href="%s">View meat</a>', 'wth'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'wth'),
		3 => __('Custom field deleted.', 'wth'),
		4 => __('Meat updated.', 'wth'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Meat restored to revision from %s', 'wth'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Meat published. <a href="%s">View meat</a>', 'wth'), esc_url( $permalink ) ),
		7 => __('Meat saved.', 'wth'),
		8 => sprintf( __('Meat submitted. <a target="_blank" href="%s">Preview meat</a>', 'wth'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Meat scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview meat</a>', 'wth'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Meat draft updated. <a target="_blank" href="%s">Preview meat</a>', 'wth'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'meat_updated_messages' );

// hook into the init action and call create_meat_taxonomies when it fires
add_action( 'init', 'create_meat_taxonomy', 0 );

// create taxonomy the post type "meat"
function create_meat_taxonomy() {

	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Cuts', 'taxonomy general name' ),
		'singular_name'     => _x( 'Cut', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Cuts' ),
		'all_items'         => __( 'All Cuts' ),
		'parent_item'       => __( 'Parent Cut' ),
		'parent_item_colon' => __( 'Parent Cut:' ),
		'edit_item'         => __( 'Edit Cut' ),
		'update_item'       => __( 'Update Cut' ),
		'add_new_item'      => __( 'Add New Cut' ),
		'new_item_name'     => __( 'New Cut Name' ),
		'menu_name'         => __( 'Cut' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'cut' ),
	);

	register_taxonomy( 'cut', array( 'meat' ), $args );

}
