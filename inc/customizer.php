<?php
/**
 * CHESTERTON Theme Customizer
 *
 * @package CHESTERTON
 */


/**
 * Default colors.
 */

$text_header = '#121212';
$text_menu = '#444444';
$text_hover_menu = '#222222';

$text_content = '#121212';
$text_sidebar = '#121212';
$text_footer = '#121212';

$bkgnd_body = '#ffffff';
$bkgnd_menu = '#cccccc';
$bkgnd_content = '#ffffff';
$bkgnd_sidebar = '#ffffff';
$bkgnd_footer = '#ffffff';
$bkgnd_body = '#ffffff';


/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function chesterton_customize_register( $wp_customize ) {


	global $text_header;
	global $text_menu;
	global $text_hover_menu;

	global $text_content;
	global $text_sidebar;
	global $text_footer;

	global $bkgnd_body;
	global $bkgnd_menu;
	global $bkgnd_content;
	global $bkgnd_sidebar;
	global $bkgnd_footer;
	global $bkgnd_body;

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
//	$wp_customize->get_setting( 'headercolor' )->transport = 'postMessage';

	// remove defatult sections
	if ( 0)
	{
		$wp_customize->remove_section('title_tagline');
		$wp_customize->remove_section('colors');
		$wp_customize->remove_section('header_image');
		$wp_customize->remove_section('background_image');
		$wp_customize->remove_section('nav');
		$wp_customize->remove_panel('widgets');
		$wp_customize->remove_section('menus');
		$wp_customize->remove_section('static_front_page');
	}

	$wp_customize->add_setting( 'menu_backgroundcolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $bkgnd_menu,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_backgroundcolor',
		array(
	    'label' => __( 'Menu Background Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );


	$wp_customize->add_setting( 'menucolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $text_menu,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menucolor',
		array(
	    'label' => __( 'Menu Text Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );

	$wp_customize->add_setting( 'menu_hovercolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $text_hover_menu,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_hovercolor',
		array(
	    'label' => __( 'Menu Button Hover Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );


	$wp_customize->add_setting( 'body_backgroundcolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $bkgnd_body,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_backgroundcolor',
		array(
	    'label' => __( 'Content Background Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );


	$wp_customize->add_setting( 'bodycolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $text_content,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bodycolor',
		array(
	    'label' => __( 'Content Text Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );

	$wp_customize->add_setting( 'sidebar_backgroundcolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $bkgnd_sidebar,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_backgroundcolor',
		array(
	    'label' => __( 'Sidebar Background Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );

	$wp_customize->add_setting( 'sidebarcolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $text_sidebar,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebarcolor',
		array(
	    'label' => __( 'Sidebar Text Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );



	$wp_customize->add_setting( 'footer_backgroundcolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $bkgnd_footer,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_backgroundcolor',
		array(
	    'label' => __( 'Footer Background Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );

	$wp_customize->add_setting( 'footercolor',
		array(
	    'type' => 'theme_mod',
	    'default' => $text_footer,
	    'transport' => 'postMessage', // or postMessage
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footercolor',
		array(
	    'label' => __( 'Footer Text Color', 'chesterton' ),
	    'section' => 'colors',
	) ) );



	$wp_customize->add_section( 'chesterton_copyright_message_section', array(
		'title'          => 'Copyright Message Settings',
		//'priority'       => 160,
		'description' => '<b>' . __( 'Copyright message:', 'chesterton' ) .'</b>' ,
	) );
	$wp_customize->add_setting( 'chesterton_copyright_message', array(
		'type'    => 'theme_mod',
		'default' => '© Copyright 2015',
    	'transport' => 'postMessage',
	) );
	$wp_customize->add_control( 'chesterton_copyright_message', array(
		'section' => 'chesterton_copyright_message_section',
		'label' => __('Copyright Text', 'chesterton'),
		'settings'   => 'chesterton_copyright_message',
		'type'    => 'text',
	) );



}
add_action( 'customize_register', 'chesterton_customize_register' );


function chesterton_customize_css()
{

	global $text_header;
	global $text_menu;
	global $text_hover_menu;

	global $text_content;
	global $text_sidebar;
	global $text_footer;

	global $bkgnd_body;
	global $bkgnd_menu;
	global $bkgnd_content;
	global $bkgnd_sidebar;
	global $bkgnd_footer;
	global $bkgnd_body;

	?>
	<style type="text/css">

		.navbar-default .navbar-nav > li > a,
		.navbar-default .navbar-nav .open .dropdown-menu > li > a  {
			color: <?php echo get_theme_mod( 'menucolor', $text_menu ); ?>;
		}
		.navbar-default,
		.dropdown-menu,
		.navbar-default .navbar-nav .open .dropdown-menu > ul,  
		.navbar-default .navbar-nav .open .dropdown-menu > li > a  {
			background-color: <?php echo get_theme_mod( 'menu_backgroundcolor', $bkgnd_menu ); ?>;
			border-color: <?php echo get_theme_mod( 'menu_backgroundcolor', $bkgnd_menu ); ?>;
		}

		.dropdown-menu	{
			border-color: <?php echo get_theme_mod( 'menu_backgroundcolor', $bkgnd_menu ); ?>;
		}		
		
		.navbar-default .navbar-nav .open .dropdown-menu > li > a:hover   {
			background-color: <?php echo get_theme_mod( 'menu_hovercolor', $text_hover_menu ); ?>;
		}

		#main {
			background-color: <?php echo get_theme_mod( 'body_backgroundcolor', $bkgnd_content ); ?>;
			color: <?php echo get_theme_mod( 'bodycolor', $text_content ); ?>;
		}

		#secondary, #content {
			background-color: <?php echo get_theme_mod( 'sidebar_backgroundcolor', $bkgnd_sidebar ); ?>;
			color: <?php echo get_theme_mod( 'sidebarcolor', $text_sidebar ); ?>;
		}

		.site-footer, .site-footer .fa {
			background-color: <?php echo get_theme_mod( 'footer_backgroundcolor', $bkgnd_footer ); ?>;
			color: <?php echo get_theme_mod( 'footercolor', $text_footer ); ?>;
		}

		</style>
	<?php
}
add_action( 'wp_head', 'chesterton_customize_css');
/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function chesterton_customize_preview_js() {
	wp_enqueue_script( 'chesterton_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '4'.date( '.YmdGi'), true );
}

add_action( 'customize_preview_init', 'chesterton_customize_preview_js' );
