<?php
/**
 * Utility functions that act independently of the theme templates
 *
 * @package WTH
 */

// debug
function print_rs( $var)
{
	echo '<div style="background-color:black;color:white;padding-top:90px;">'. str_replace( ')', ')<br />', str_replace( 'Array', '<br />Array', str_replace( '[', '<br />[', print_r( $var, true)))) . '</div>';
}

function SendEmail( $to, $subject, $body)
    {

    apply_filters( 'wp_mail_content_type', 'text/html');

    $company = get_option( 'wth-title1').' '.get_option( 'chesterton-title2');
    $email = get_option( 'wth-email' );

    $headers = "From: $company <$email>"."\r\n\\";
    wp_mail( $to, $subject, $body, $headers);

//echo "<br />Sending email  FROM:$headers; TO:$to; SUBJECT:$subject; BODY:$body";
//echo "<br />";
    }


function GetPostInput( &$input, $name, $default='')
    {
    if (  isset( $_POST[ "$name"]))
        $input = $_POST[ "$name"];
    else
        $input = $default;
    }

function GetPostInputCheckbox( &$input, $name)
    {
    if (  isset( $_POST[ "$name"]))
        $input = 1;
    else
        $input = 0;
    }

// encode funky HTML data before storing into database
// prevent mysql insertion attack
// EncodeForMySQL
function StrEncode( $str)
    {
    // adds slashes
    $str = mysql_real_escape_string( $str);

    return( $str);
    }

// decode funky HTML from GET/POST input variable
// DecodeFormInputFromServer
function StrDecode( $str)
    {
    $str = stripslashes( $str);

    return( $str);
    }


// encode funky HTML data before displaying in a form element
// EncodeForFormFields
function StrHTMLEntities( $str)
    {
//    return( $str);
    // handle special chracters for HTML display, form text-input

    // just translate problem characters, special characters stay the same.
    $oldStr = preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);
    $newStr = '';
    foreach ( $oldStr as $ch)
        {
        if ( $ch == "'" || $ch == '"')
            $ch = htmlentities( $ch, ENT_QUOTES);
        else
            ;
        $newStr .= $ch;
        }
    $str = $newStr;

    return( $str);
    }


function ProcessInput( $fieldName, $data)
    {
    GetGetInput( $data, $fieldName);
    $data = StrDecode( $data);

    return( $data);
    }


function ValidateEmail( $email)
    {
    // First, we check that there's one @ symbol, and that the lengths are right
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email))
        {
        // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
        return false;
        }
    // Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++)
        {
        if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i]))
            {
            return false;
            }
        }
    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1]))
        { // Check if domain is IP. If not, it should be valid domain name
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2)
            {
            return false; // Not enough parts to domain
            }
        for ($i = 0; $i < sizeof($domain_array); $i++)
            {
            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i]))
                {
                return false;
                }
            }
        }
    return true;
    }


function ValidateEmailInput( &$data, &$errorMsg)
    {
    $len = strlen( $data);
    if ( $len >= 250)
        {
        $errorMsg = "Too many characters.";
        return( false);
        }

    if ( ValidateEmail( $data) == false)
        {
        $errorMsg = "Invalid email address.";
        return( false);
        }


    return( true);
    }



function ValidateTextInput( &$data, &$errorMsg, $min=0)
    {
    $len = strlen( $data);
    if ( $len >= 255)
        {
        $errorMsg = "Too long (>254 characters).";
        return( false);
        }
    if ( $min && $len == 0)
        {
        $errorMsg = "Required.";
        return( false);
        }
    return( true);
    }


function ValidateBigTextInput( &$data, &$errorMsg, $min=0)
    {
    $len = strlen( $data);
    if ( $len >= 1024)
        {
        $errorMsg = "Too long (> 1024 characters).";
        return( false);
        }
    if ( $min && $len == 0)
        {
        $errorMsg = "Required.";
        return( false);
        }
    return( true);
    }

function ValidatePasswordInput( &$data, &$errorMsg, $min=0)
    {
    $len = strlen( $data);
    if ( $len >= 255)
        {
        $errorMsg = "Too long (>254 characters).";
        return( false);
        }
    if ( $min && $len == 0)
        {
        $errorMsg = "Required.";
        return( false);
        }
    else if ( $len < $min)
        {
        $errorMsg = "Must be at least $min characters long.";
        return( false);
        }

    // characters allowed
    if ( ctype_graph( $data) == false)
        {
        $errorMsg = "Illegal characters.";
        return( false);
        }

    return( true);
    }


function ValidateUsernameInput( &$data, &$errorMsg, $min=0)
    {
    $len = strlen( $data);
    if ( $len >= 255)
        {
        $errorMsg = "Too long (>254 characters).";
        return( false);
        }
    if ( $min && $len == 0)
        {
        $errorMsg = "Required.";
        return( false);
        }

    // see if username already exists
    if ( $len < $min)
        {
        $errorMsg = "Must be at least $min characters long.";
        return( false);
        }


    return( true);
    }


function ValidateIntegerInput( &$data, &$errorMsg)
    {

    if ( is_numeric( $data) == false)
        {
        $errorMsg = "Must be a number.";
        return( false);
        }
    return( true);
    }





function GetScandir( $dir)
    {

    $files = array();

    $dh  = opendir($dir);
    $filename = readdir($dh);
    while ( (false !== ($filename = readdir($dh))) )
        {
        $path = $dir . '/' . $filename;
//echo "path:$path, <br />";
        if( is_dir( $path) == false)
            $files[] = $filename;
//echo "fn:$filename,<br />";
//        $filename = readdir($dh);
        }

    sort($files);

    return( $files);
    }


// create a list str separated with pipes
function GetSelectListDirUploads( $subDir='')
    {

    $target = "wp-content/uploads";

    global $dl_filepath;

    $target = $dl_filepath;

//echo"target:$target<br />";

    if ( $subDir)
        $target .= $subDir . "/";
    $list = GetScandir( $target);

    // put in form we want
    $str = implode( "|", $list);

    return( $str);
    }


// build an array list with WP menu items ordered into single level dropdowns
function BuildMenuListForDropdowns( $menu_items)
{
    $menu_list = array();

    $i = -1;
    if ( $menu_items)
    foreach ( $menu_items as $mi)
    {
        $i++;
        $menu_list[ $i][ 'title'] = $mi->title;
        $menu_list[ $i][ 'url'] = $mi->url;
        $menu_list[ $i][ 'object_id'] = $mi->object_id;

        // singles, or parents
        if ( $mi->menu_item_parent == 0)
        {
            $menu_list[ $i][ 'is_child'] = false;
            if ( $i != 0)
            {
                // fix previous child list
                if ( $menu_list[ $i-1][ 'is_child'])
                    $menu_list[ $i-1][ 'last_child'] = true;
            }
            $menu_list[ $i][ 'has_child'] = false;  // maybe
            $menu_list[ $i][ 'last_child'] = false;

        }
        // children
        else
        {
            // previous parent
            if ( $menu_list[ $i-1][ 'is_child'] == false)
                $menu_list[ $i-1][ 'has_child'] = true;
            $menu_list[ $i][ 'is_child'] = true;

            $menu_list[ $i][ 'has_child'] = false;
            $menu_list[ $i][ 'last_child'] = false;
        }
    }
    if ( $i != 0)
    {
        // fix previous child list
        if ( $menu_list[ $i][ 'is_child'])
            $menu_list[ $i][ 'last_child'] = true;
    }

    return( $menu_list);
}


function GetNavMenuBootstrapDropdowns( $menu, $ptitle = '')
{
   	$menu = wp_get_nav_menu_object( $menu );
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    $menu_list = BuildMenuListForDropdowns( $menu_items);

    $site_name = 'Bootstrap theme';
    $s = '';

    $option = get_option( 'eeb-external-map-link');
    $url = get_bloginfo( 'url');
    $logo_url = get_template_directory_uri().'/images/'.'logo-employee-perfromance-solutions.png';
    $home_url = esc_url( home_url( '/' ) );

    if ( $option = get_option( 'eeb-external-map-link'))
    	$map_link = '<span class="navbar-toggle navbar-toggle-text" data-toggle="collapse" data-target=".navbar-collapse"><a href="'. $option. '" target="_blank"><i class="fa fa-map-marker"></i> Map</a></span>';
    else
    	$map_link = '';

    // opening css
    $s .=<<< GNMBD1
    <!-- Fixed navbar CSS -->

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">

GNMBD1;

	// now add <li> elements
    foreach ( $menu_list as $m)
    {
        if ( strip_tags( strtolower( $ptitle)) == strip_tags( strtolower( $m['title'])))
            $active = ' class="active"';
        else
            $active = '';

        $u = $m['url'];
        $t = $m['title'];
//echo $t.'<br />';
        if ( $m[ 'has_child'])
        {
            // dropdown css
            $s .=<<< GNMBD2
                    <li class="dropdown">
                      <a href="{$u}" class="dropdown-toggle" data-toggle="dropdown">{$t} <br /><b class="caret"></b></a>
                      <ul class="dropdown-menu dropdown-menu-left">
GNMBD2;
            // add redundant clickable link
            $s .= '
            <li'.$active.'><a href="'.$u.'">'.$t.'</a></li>';
        }
        else
            $s .= '
            <li'.$active.'><a href="'.$u.'">'.$t.'</a></li>';


        if ( $m[ 'last_child'])
        {
            // close dropdown css
            $s .=<<< GNMBD3
              </ul>
            </li>
GNMBD3;
        }
    }

    $url = get_bloginfo('url');
    // close it
    $s .=<<< GNMBD4
          </ul>
          <!-- search form
          <form class="navbar-form navbar-right searchform" role="search" method="get" id="searchform" action="{$url}">
            <div class="form-group">
              <input type="text"  name="s" id="s" placeholder="Search" class="form-control">
            </div>
            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
          </form>
          -->
    </div>
GNMBD4;

    return ( $s);
}


