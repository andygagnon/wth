<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WTH
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'wth' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'wth' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'wth' ), 'wth', '<a href="http://andregagnon.com/" rel="designer">Andre Gagnon</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div> <!-- end container -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
